﻿namespace RobinBird.Abstractions.Database.Interfaces
{
    using System;

    public interface IDatabase
    {
        void Login(IDatabaseConnectionInfo connectionInfo, Action<bool> resultHandler);
    }
}