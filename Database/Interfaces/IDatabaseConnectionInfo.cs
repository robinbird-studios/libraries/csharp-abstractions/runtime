﻿namespace RobinBird.Abstractions.Database.Interfaces
{
    public interface IDatabaseConnectionInfo
    {
        string User { get; set; }
        string Password { get; set; }
        string DatabaseUri { get; set; }
        string DatabaseName { get; set; }
    }
}