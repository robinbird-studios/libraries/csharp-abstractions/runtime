﻿#region Disclaimer

// <copyright file="IResponse.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>

#endregion

namespace RobinBird.Abstractions.Http.Interfaces
{
    using System.Net;

    public interface IResponse
    {
        HttpStatusCode Status { get; set; }

        string Text { get; set; }
    }
}