﻿#region Disclaimer

// <copyright file="IRestClient.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>

#endregion

namespace RobinBird.Abstractions.Http.Interfaces
{
    using System;

    public interface IRestClient
    {
        void AddCustomRequestHeader(string key, string value);

        void RemoveCustomRequestHeader(string key);

        void Send(IRequest request, RequestReturnedDelegate responseCallback);

        void Get(string uri, RequestReturnedDelegate responseCallback);

        void Post(string uri, string text, RequestReturnedDelegate responseCallback);

        void Post(string uri, object data, Type type, RequestReturnedDelegate responseCallback);

        void Post<T>(string uri, T data, RequestReturnedDelegate responseCallback);
    }
}