﻿#region Disclaimer

// <copyright file="IRequest.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>

#endregion

namespace RobinBird.Abstractions.Http.Interfaces
{
    using System;

    public interface IRequest
    {
        string Method { get; set; }
        string Text { get; set; }
        Uri Uri { get; set; }

        void Initialize(string method, string uri);

        void AddHeader(string key, string value);

        string GetHeader(string key);

        void Send(RequestReturnedDelegate responseCallback);
    }

    public delegate void RequestReturnedDelegate(IRequest sentRequest, IResponse response);
}