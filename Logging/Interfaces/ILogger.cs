﻿namespace RobinBird.Abstractions.Logging.Interfaces
{
    public interface ILogger
    {
        void Info(string message);
        void Info(string message, object context);


        void Warn(string message);
        void Warn(string message, object context);


        void Error(string message);
        void Error(string message, object context);
    }
}