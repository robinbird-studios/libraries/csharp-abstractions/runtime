﻿namespace RobinBird.Abstractions.Logging.Interfaces
{
    /// <summary>
    /// Implement to log to system console
    /// </summary>
    public interface IConsoleLogger : ILogger
    {
    }
}