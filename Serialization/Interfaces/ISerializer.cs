﻿namespace RobinBird.Abstractions.Serialization.Interfaces
{
    /// <summary>
    /// General interface for any serializer.
    /// </summary>
    public interface ISerializer
    {
        /// <summary>
        /// Http style content type that this serializer accepts and creates.
        /// </summary>
        string HttpContentType { get; }
    }
}