﻿#region Disclaimer

// <copyright file="IPersistence.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>

#endregion

namespace RobinBird.Abstractions.Serialization.Interfaces
{
    using System;
    using System.IO;

    /// <summary>
    /// Interface to serialize data to file or deserialize data from file to instance.
    /// </summary>
    public interface IFileSerializer : ISerializer
    {
        /// <summary>
        /// Extension of the file to serialize or deserialize. Returns pure extension without dot.
        /// (e.g. "xml", "json", etc.)
        /// </summary>
        string Extension { get; }

        /// <summary>
        /// Serialize data to disk.
        /// </summary>
        /// <param name="path">Destination of the serialized file. Will override existing file.</param>
        /// <param name="o">Data to serialize.</param>
        /// <param name="type">The type of the data to serialize.</param>
        /// <exception cref="ArgumentOutOfRangeException"><seealso cref="path" /> is invalid, null or empty.</exception>
        void SerializeToFile(string path, object o, Type type);


        /// <summary>
        /// Serialize data to disk.
        /// </summary>
        /// <typeparam name="T">The type of the data to serialize.</typeparam>
        /// <param name="path">Destination of the serialized file. Will override existing file.</param>
        /// <param name="o">Data to serialize.</param>
        /// <exception cref="ArgumentOutOfRangeException"><seealso cref="path" /> is invalid, null or empty.</exception>
        void SerializeToFile<T>(string path, object o);


        /// <summary>
        /// Returns deserialized data from file given with <seealso cref="path" /> parameter.
        /// </summary>
        /// <param name="path">Path to the file to deserialize.</param>
        /// <param name="type">Type of the data to deserialize.</param>
        /// <exception cref="FileNotFoundException"><seealso cref="path" /> is null, empty or does not exist.</exception>
        object DeserializeFromFile(string path, Type type);


        /// <summary>
        /// Returns deserialized data from file given with <seealso cref="path" /> parameter.
        /// </summary>
        /// <typeparam name="T">Type of the data to deserialize.</typeparam>
        /// <param name="path">Path to the file to deserialize.</param>
        /// <exception cref="FileNotFoundException"><seealso cref="path" /> is null, empty or does not exist.</exception>
        T DeserializeFromFile<T>(string path);
    }
}