﻿namespace RobinBird.Abstractions.Serialization.Interfaces
{
    using System;

    /// <summary>
    /// Serializer to serialize data to text or deserialize text to object instance.
    /// </summary>
    public interface ITextSerializer : ISerializer
    {
        /// <summary>
        /// Serialize instance <paramref name="o" /> of type <typeparamref name="T" /> to text.
        /// </summary>
        string SerializeToString<T>(T o);

        /// <summary>
        /// Serialize instance <paramref name="o" /> of type <paramref name="type" /> to text.
        /// </summary>
        string SerializeToString(object o, Type type);

        /// <summary>
        /// Deserialize text to to instance of type <typeparamref name="T" />.
        /// </summary>
        T DeserializeFromString<T>(string data);

        /// <summary>
        /// Deserialize text to to instance of type <paramref name="type" />.
        /// </summary>
        object DeserializeFromString(string data, Type type);
    }
}