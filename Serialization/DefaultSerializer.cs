﻿#region Disclaimer

// <copyright file="Persistence.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>

#endregion

namespace RobinBird.Abstractions.Serialization
{
    using System;
    using System.IO;
    using Interfaces;

    /// <summary>
    /// Wrapper class that abstracts different serialization methods.
    /// It makes it easy to switch serializers and makes it easy to call to serialization because serializer
    /// specific methods don't have to be remembered.
    /// </summary>
    public abstract class DefaultSerializer : IFileSerializer, ITextSerializer
    {
        /// <summary>
        /// Returns string with serializer specific extension.
        /// </summary>
        protected string AddExtensionToPath(string path)
        {
            string pathWithExtension = string.Format("{0}.{1}", path, Extension);

            if (string.IsNullOrEmpty(pathWithExtension))
            {
                throw new ArgumentOutOfRangeException("path", "Path cannot be empty or null");
            }
            return pathWithExtension;
        }

        /// <summary>
        /// Create the directory for a given <paramref name="path" />.
        /// </summary>
        protected void CreateDirectoryForPath(string path)
        {
            // Create directories
            string dir = Path.GetDirectoryName(path);
            if (string.IsNullOrEmpty(dir) == false)
            {
                Directory.CreateDirectory(dir);
            }
        }

        #region IFileSerializer Implementation

        /// <summary>
        /// Serialize data to disk.
        /// </summary>
        /// <param name="path">Destination of the serialized file. Will override existing file.</param>
        /// <param name="o">Data to serialize.</param>
        /// <param name="type">The type of the data to serialize.</param>
        /// <exception cref="ArgumentOutOfRangeException"><seealso cref="path" /> is null or empty.</exception>
        public abstract void SerializeToFile(string path, object o, Type type);

        /// <summary>
        /// Serialize data to disk. This overload is using a custom provider instead of the global one.
        /// </summary>
        /// <typeparam name="T">The type of the data to serialize.</typeparam>
        /// <param name="path">Destination of the serialized file. Will override existing file.</param>
        /// <param name="o">Data to serialize.</param>
        /// <exception cref="ArgumentOutOfRangeException"><seealso cref="path" /> is null or empty.</exception>
        public void SerializeToFile<T>(string path, object o)
        {
            SerializeToFile(path, o, typeof (T));
        }

        /// <summary>
        /// Extension of the file to serialize or deserialize. Returns pure extension without dot.
        /// (e.g. "xml", "json", etc.)
        /// </summary>
        public abstract string Extension { get; }

        /// <summary>
        /// Returns deserialized data from file given with <seealso cref="path" /> parameter.
        /// </summary>
        /// <param name="path">Path to the file to deserialize.</param>
        /// <param name="type">Type of the data to deserialize.</param>
        /// <exception cref="FileNotFoundException"><seealso cref="path" /> is null/empty or does not exist.</exception>
        public abstract object DeserializeFromFile(string path, Type type);

        /// <summary>
        /// Returns deserialized data from file given with <seealso cref="path" /> parameter.
        /// </summary>
        /// <typeparam name="T">Type of the data to deserialize.</typeparam>
        /// <param name="path">Path to the file to deserialize.</param>
        /// <exception cref="FileNotFoundException"><seealso cref="path" /> is null/empty or does not exist.</exception>
        public T DeserializeFromFile<T>(string path)
        {
            return (T) DeserializeFromFile(path, typeof (T));
        }

        #endregion

        #region ISerializer Implementation

        /// <summary>
        /// Http style content type that this serializer accepts and creates.
        /// </summary>
        public abstract string HttpContentType { get; }

        #endregion

        #region ITextSerializer Implementation

        /// <summary>
        /// Serialize instance <paramref name="o" /> of type <typeparamref name="T" /> to text.
        /// </summary>
        public string SerializeToString<T>(T o)
        {
            return SerializeToString(o, typeof (T));
        }

        /// <summary>
        /// Serialize instance <paramref name="o" /> of type <paramref name="type" /> to text.
        /// </summary>
        public abstract string SerializeToString(object o, Type type);

        /// <summary>
        /// Deserialize text to to instance of type <typeparamref name="T" />.
        /// </summary>
        public T DeserializeFromString<T>(string data)
        {
            return (T) DeserializeFromString(data, typeof (T));
        }

        /// <summary>
        /// Deserialize text to to instance of type <paramref name="type" />.
        /// </summary>
        public abstract object DeserializeFromString(string data, Type type);

        #endregion
    }
}