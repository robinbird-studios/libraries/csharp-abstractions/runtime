﻿#region Disclaimer

// <copyright file="PersistenceIgnoreAttribute.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>

#endregion

namespace RobinBird.Abstractions.Serialization.Attributes
{
    using System;

    /// <summary>
    /// Attribute to specify that a specific member or field should not be serialized.
    /// This member will be replaced by dll based dependency injection.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class IgnoreAttribute : Attribute
    {
    }
}